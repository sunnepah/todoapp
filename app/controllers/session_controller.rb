class SessionController < ApplicationController
  def new
    render :login
  end
  def create
    redirect_to tasks_path
  end
end
