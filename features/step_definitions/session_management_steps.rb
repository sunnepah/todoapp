Given(/^the user "(.*?)" has an account$/) do |username|
  @user = User.create!(username: username, password: 'password')
end

When(/^he logs in$/) do
  visit login_path
  fill_in "Username", with: @user.username
  fill_in "Password", with: @user.password
  click_button "Log in"
end

Then(/^he should see "(.*?)"$/) do |message|
  expect(page).to have_content(message)
end

Given(/^the user "(.*?)" does not have an account$/) do |arg1|
  pending # express the regexp above with the code you wish you had
end

When(/^then he logs out$/) do
  pending # express the regexp above with the code you wish you had
end
